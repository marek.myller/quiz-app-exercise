const questions = [
  {
    id: 1,
    question: 'What is the capital of France?',
    options: ['London', 'Paris', 'Madrid', 'Berlin'],
    answer: 'Paris',
  },
  {
    id: 2,
    question: 'What is the highest mountain in the world?',
    options: ['Mount Everest', 'K2', 'Makalu', 'Kangchenjunga'],
    answer: 'Mount Everest',
  },
  {
    id: 3,
    question: 'What is the largest continent in the world?',
    options: ['Africa', 'South America', 'Asia', 'Europe'],
    answer: 'Asia',
  },
  {
    id: 4,
    question: 'JavaScript is fun?',
    options: ['Yes', 'No'],
    answer: 'Yes',
  },
];
const instructions = document.querySelector('.instructions');
const NewGameBtn = document.querySelector('.btn');
let score = 0;

const createAndInsertText = (elementName, text) => {
  const element = document.createElement(elementName);
  element.innerText = text;
  return element;
};

const createQuizRemaining = idPosition => {
  let remaining = questions.length + 1 - idPosition;
  if (remaining == 1) {
    return `Last question!`;
  }
  return `${remaining} questions to go!`;
};

const createOptionsAndPickAnswerAndScore = (options, answer, id) => {
  const optionUl = document.createElement('ul');
  for (let i = 0; i < options.length; i++) {
    const option = document.createElement('li');
    option.innerText = options[i];
    optionUl.append(option);
    option.addEventListener('dblclick', function () {
      if (options[i] == answer) {
        score = score + 1;
      }
      this.parentNode.parentNode.remove();
      if (document.querySelector('.displayArea').innerHTML == '') {
        document.querySelector('.score').innerHTML = `Score: ${score}`;
        document.querySelector('.score').classList.remove('hidden');
        instructions.classList.add('hidden');
        NewGameBtn.classList.remove('hidden');
      }
    });
  }
  return optionUl;
};

const displayQuestion = questionsToDisplay => {
  const questionsFragment = document.createDocumentFragment();

  questionsToDisplay.forEach(questions => {
    const questionElement = document.createElement('li');
    questionElement.append(createQuizRemaining(questions.id));

    questionElement.append(createAndInsertText('h2', questions.question));
    questionElement.append(
      createOptionsAndPickAnswerAndScore(questions.options, questions.answer)
    );
    questionsFragment.append(questionElement);
  });
  document.querySelector('.displayArea').append(questionsFragment);
};
NewGameBtn.addEventListener('click', function () {
  score = 0;
  document.querySelector('.score').classList.add('hidden');
  document.querySelector('.displayArea').innerText = '';
  instructions.classList.remove('hidden');
  NewGameBtn.classList.add('hidden');

  displayQuestion(questions);
});
displayQuestion(questions);
